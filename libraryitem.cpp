#include "libraryitem.h"

LibraryItem::LibraryItem(QObject *parent) : QAbstractItemModel(parent) {
    parentItem = static_cast<LibraryItem*>(parent);
    itemData.reserve(2); // Réservation d'espace mémoire pour le titre et la description
}

LibraryItem::LibraryItem(const QList<QPair<QString, QVariant>> &data, LibraryItem *parentCard)
    : LibraryItem(parentCard) {
    itemData = data;
}

LibraryItem::LibraryItem(const LibraryItem &other, LibraryItem *parentCard)
    : LibraryItem(other.itemData, parentCard) {
}

LibraryItem::~LibraryItem() {
    qDeleteAll(children);
}

bool LibraryItem::operator==(const LibraryItem& other) const {
    if (typeid(*this) != typeid(other))
        return false;

    return itemData == other.itemData;
}

// --------------------------------------------

QModelIndex LibraryItem::index(int row, int column, const QModelIndex &parentIndex) const {
    if (!hasIndex(row, column, parentIndex))
        return QModelIndex();

    LibraryItem *parentCard;

    if (!parentIndex.isValid())
        parentCard = parentItem;
    else
        parentCard = static_cast<LibraryItem*>(parentIndex.internalPointer());

    LibraryItem *childCard = parentCard->child(row);

    if (childCard)
        return createIndex(row, column, childCard);

    return QModelIndex();
}

QModelIndex LibraryItem::parent(const QModelIndex &index) const {
    if (!index.isValid())
        return QModelIndex();

    LibraryItem *child = static_cast<LibraryItem*>(index.internalPointer());
    LibraryItem *parent = child->parentCard();

    if (parent == parentItem)
        return QModelIndex();

    return createIndex(parent->row(), 0, parent);
}

// --------------------------------------------

int LibraryItem::rowCount(const QModelIndex &parent) const {
    if (parent.column() > 0)
        return 0;
    if (!parent.isValid())
        return children.count();

    return static_cast<LibraryItem*>(parent.internalPointer())->rowCount();
}

int LibraryItem::columnCount(const QModelIndex &parent) const {
    if (parent.isValid())
        return static_cast<LibraryItem*>(parent.internalPointer())->columnCount();

    return itemData.count();
}

// --------------------------------------------

QVariant LibraryItem::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    return itemData.value(index.column()).second;
}

bool LibraryItem::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (index.isValid() && role == Qt::EditRole) {
        if (0 > index.column() || index.column() >= itemData.count())
            return false;

        itemData[index.column()].second = value;
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

// --------------------------------------------

Qt::ItemFlags LibraryItem::flags(const QModelIndex &index) const {
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

QVariant LibraryItem::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return parentItem->data(section);

    return QVariant();
}

// --------------------------------------------

void LibraryItem::appendChild(LibraryItem *childCard) {
    children.append(childCard);
}

bool LibraryItem::removeChild(const int &row) {
    if (row < 0 || row >= children.count())
        return false;

    children.removeAt(row);
    return true;
}

LibraryItem *LibraryItem::child(const int &row) {
    return children.value(row);
}

LibraryItem *LibraryItem::parentCard() {
    return parentItem;
}

// --------------------------------------------

int LibraryItem::row() const {
    if (parentItem)
        return parentItem->children.indexOf(const_cast<LibraryItem*>(this));

    return 0;
}

QVariant LibraryItem::data(int column) const {
    return itemData.value(column).second;
}

// --------------------------------------------

bool LibraryItem::isCard() {
    return (parentItem && parentItem->isCardPack());
}

bool LibraryItem::isCardPack() {
    return (parentItem && !parentItem->parentCard());
}

// --------------------------------------------

void LibraryItem::addData(const QString &fieldName, const QVariant &data) {
    addDataAt(itemData.count(), fieldName, data);
}

void LibraryItem::addDataAt(const int &column, const QString &fieldName, const QVariant &data) {
    if (column < 0)
        return addData(fieldName, data);

    QModelIndex cardIndex = createIndex(row(), 0, parentCard());

    if (itemData.count() > column) { // Trop de colonnes : remplacement de la donnée
        itemData.replace(column, QPair<QString, QVariant>(fieldName, data));
        return;
    }

    if (itemData.count() < column) { // Trop peu de colonnes : ajout de données vide
        beginInsertColumns(cardIndex, itemData.count(), column - itemData.count());
        for (int i = itemData.count(); i < column - itemData.count(); i++)
            itemData.append(QPair<QString, QVariant>("", QVariant()));
        endInsertColumns();
    }

    // Nombre exact de colonnes : ajout de la donnée
    beginInsertColumns(cardIndex, column, column);
    itemData.append(QPair<QString, QVariant>(fieldName, data));
    endInsertColumns();
}

void LibraryItem::setTitle(const QString &title) {
    addDataAt(0, "Title", title);
}

void LibraryItem::setDescription(const QString &description) {
    addDataAt(1, "Description", description);
}

// --------------------------------------------

QString LibraryItem::getFieldName(const int &i) const {
    return itemData.value(i).first;
}

QVariant LibraryItem::getFieldData(const int &i) const {
    return itemData.value(i).second;
}

QString LibraryItem::getTitle() const {
    return getFieldData(0).toString();
}

QString LibraryItem::getDescription() const {
    return getFieldData(1).toString();
}
