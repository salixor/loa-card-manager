#include "mainwindow.h"
#include <QApplication>
#include <QSettings>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setApplicationName("Flashcard Manager");
    QApplication::setOrganizationName("ENSIIE LOA");

    MainWindow w;
    w.show();

    return a.exec();
}
