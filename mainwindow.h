#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "libraryitem.h"
#include "cardwidget.h"

#include <QFileDialog>
#include <QMainWindow>
#include <QItemSelection>
#include <QSettings>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private slots:
        void updateCardDisplay();
        void updateButtons();
        void updateStatusBar(QString specialMessage = QString());

        void setModified(bool modified = true);
        void setCurrentJSONFile(QString filename = QString());

        void on_addCardButton_clicked();
        void on_actionAdd_card_triggered();

        void on_addCardPackButton_clicked();
        void on_actionAdd_card_pack_triggered();

        void on_deleteButton_clicked();

        void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

        void on_actionOpen_JSON_triggered();
        void on_actionImport_JSON_triggered();
        void on_actionSave_JSON_triggered();
        void on_actionExport_JSON_triggered();

        void on_actionClose_JSON_triggered();
        void on_actionReset_library_triggered();

    private:
        Ui::MainWindow *ui;

        QSettings settings;
        Library *library;

        bool isModified = false;

        // Informations et widgets pour le JSON ouvert actuellement
        QString currentJSONFilename = QString();
        QLabel *jsonOpenFileLabel;

        // Informations et widgets pour le nombre de cartes et paquets actuels
        int packsCount = 0;
        int cardsCount = 0;
        QLabel *packsCountLabel;
        QLabel *cardsCountLabel;

        // Widgets pour l'avant et l'arrière de l'affichage des cartes
        CardWidget *cardFront = new CardWidget;
        QWidget *cardBack = new QWidget;

        // Card et CardPack par défaut (à l'ajout d'un élément)
        LibraryItem defaultCard = new LibraryItem;
        LibraryItem defaultCardPack = new LibraryItem;

        void updateAll();

        bool isDefaultCardPack(LibraryItem *item);
        bool isDefaultCard(LibraryItem *item);

        void setAddCardEnabled(const bool &enabled);
        void setAddCardPackEnabled(const bool &enabled);

        void importJson(const QString &filename);
        void exportJson(const QString &filename, const QJsonObject json);
};

#endif // MAINWINDOW_H
