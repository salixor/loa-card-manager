#include "cardwidget.h"

CardWidget::CardWidget(QWidget *parent, Library *library) : QWidget(parent) {
    this->library = library;

    layout = new QVBoxLayout();
    mapper = new QDataWidgetMapper();
    font = QFont("Segoe UI", 14);

    addFieldButton = new QPushButton(this);
    addFieldButton->setText(QString("Add a field"));
    addFieldButton->hide();
    addFieldButton->setStyleSheet("QPushButton { margin-top: 10px; padding: 10px; }");
    connect(addFieldButton, SIGNAL(clicked(bool)), this, SLOT(addField()));

    layout->setAlignment(Qt::AlignTop);
    layout->addWidget(addFieldButton);

}

CardWidget::~CardWidget() {
    delete mapper;
}

void CardWidget::clear() {
    resetWidget();
}

void CardWidget::setCurrentIndex(QModelIndex index) {
    if (!mapper->model())
        mapper->setModel(library);

    currentIndex = index;
    mapper->setRootIndex(index.parent());

    setupFields();
}

void CardWidget::resetWidget() {
    // Nettoyage des labels
    qDeleteAll(labels);
    labels.clear();

    // Nettoyage des champs d'édition
    qDeleteAll(fields);
    fields.clear();

    addFieldButton->hide();
    mapper->clearMapping();
}

void CardWidget::setupFields() {
    if (!currentIndex.isValid())
        return;

    resetWidget();

    LibraryItem *currentItem = library->getLibraryItemAtIndex(currentIndex);

    for (int i = 0; i < library->columnCount(currentIndex); i++) {
        if (currentItem->getFieldName(i) == "")
            continue;

        QLabel *label = new QLabel(currentItem->getFieldName(i), this);
        label->setAlignment(Qt::AlignCenter);
        label->setStyleSheet("QLabel { margin: 5px; }");

        QLineEdit *edit = new QLineEdit(this);
        edit->setAlignment(Qt::AlignLeft);
        edit->setFont(font);
        edit->setStyleSheet("QLineEdit { background-color: rgba(255,255,255,0.6); padding: 10px; border: 0;  }"
                            "QLineEdit:hover, QLineEdit:focus { background-color: rgba(255,255,255,1); }");

        // Connection entre un changement de texte du champ et l'envoi au modèle
        // (avec conservation de la position du curseur)
        connect(
            edit, &QLineEdit::textEdited,
            [=]() {
                int oldCursorPosition = edit->cursorPosition();
                mapper->submit();
                edit->setCursorPosition(oldCursorPosition);
                emit(libraryElementChanged());
            }
        );

        // Insertion du label et du champ dans le layout
        layout->insertWidget(layout->count() - 1, label);
        layout->insertWidget(layout->count() - 1, edit);

        // Mapping entre le champ d'édition et la valeur
        mapper->addMapping(edit, i);

        labels.append(label);
        fields.append(edit);
    }

    if (currentItem->isCard())
        addFieldButton->show();

    setLayout(layout);
    mapper->setCurrentIndex(currentIndex.row());
}

void CardWidget::addField() {
    LibraryItem *currentItem = library->getLibraryItemAtIndex(currentIndex);
    if (!currentItem->isCard())
        return;

    // On ajoute des colonnes au CardPack pour en avoir suffisamment pour la carte
    LibraryItem *cardPack = library->getLibraryItemAtIndex(currentIndex.parent());
    for (int i = 0; i < currentItem->columnCount() + 2 - cardPack->columnCount(); i++)
        cardPack->addData();

    bool ok;
    QString fieldName = QInputDialog::getText(this, tr("New field"),
                                              tr ("Field name:"), QLineEdit::Normal,
                                              "", &ok);
    if (ok)
        currentItem->addData(fieldName, "");

    setupFields();
    emit(libraryElementChanged());
}
