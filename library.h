#ifndef CARDPACK_H
#define CARDPACK_H

#include "libraryitem.h"

#include <QAbstractItemModel>
#include <QJsonObject>
#include <QList>

class LibraryItem;

/**
 * @brief Modèle représentant la bibliothèque. Il possède une racine qui contient
 * les paquets de cartes (LibraryItem) et hérite de QAbstractItemModel pour le relier directement
 * à la vue, qui est hiérarchique (QTreeView).
 *
 */
class Library : public QAbstractItemModel {
    Q_OBJECT

    public:
        /**
         * @brief Constructeur par défaut. Initialise la racine et les trois premières
         * colonnes.
         *
         * @param parent : QObject parent utilisé par le super-constructeur de QAbstractItemModel
         */
        explicit Library(QObject *parent = nullptr);

        /**
         * @brief Destructeur. Supprime le pointeur de la racine.
         *
         */
        ~Library();

        /**
         * @brief Retourne l'index du paquet dans la librarie avec les paramètres spécifiés.
         *
         * @param row
         * @param column
         * @param parentIndex
         * @return QModelIndex
         */
        QModelIndex index(int row, int column = 0, const QModelIndex &parentIndex = QModelIndex()) const override;

        /**
         * @brief Retourne le parent du paquet/de la carte à l'index indiqué.
         *
         * @param index
         * @return QModelIndex
         */
        QModelIndex parent(const QModelIndex &index) const override;

        /**
         * @brief Retoune le nombre de paquets contenus dans la bibliothèque.
         *
         * @param parent : équivaut à la racine
         * @return int
         */
        int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        /**
         * @brief Retourne le nombre de colonnes (champs de données) pour les paquets.
         *
         * @param parent
         * @return int
         */
        int columnCount(const QModelIndex &parent = QModelIndex()) const override;

        /**
         * @brief Retourne les données associées à l'objet à l'index donné.
         *
         * @param index
         * @param role
         * @return QVariant
         */
        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        /**
         * @brief Modifie les données associées à l'objet à l'index donné.
         * Méthode nécessaire pour l'édition.
         *
         * @param index
         * @param value
         * @param role
         * @return bool
         */
        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

        void resetModel();

        /**
         * @brief Retourne les flags du paquet/de la carte à l'index donné.
         *
         * @param index
         * @return Qt::ItemFlags
         */
        Qt::ItemFlags flags(const QModelIndex &index) const override;

        /**
         * @brief Retourne les données d'en-tête pour le rôle et la section donnés.
         *
         * @param section
         * @param orientation
         * @param role
         * @return QVariant
         */
        QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

        /**
         * @brief Ajoute une carte dans le paquet donné.
         *
         * @param card
         * @param cardPack
         * @return QModelIndex
         */
        QModelIndex addCard(const LibraryItem &card, LibraryItem *cardPack = nullptr);

        /**
         * @brief Ajoute un paquet de cartes à la racine de la bibliothèque.
         *
         * @param cardPack
         * @return QModelIndex
         */
        QModelIndex addCardPack(const LibraryItem &cardPack);

        /**
         * @brief Supprime un paquet/une carte de l'objet parent.
         *
         * @param row
         * @param parentIndex
         * @return bool
         */
        bool removeRow(int row, const QModelIndex &parentIndex = QModelIndex());
        bool removeRows(int row, int count, const QModelIndex &parentIndex = QModelIndex());

        /**
         * @brief Retourne le paquet de cartes contenu à la ligne donnée.
         *
         * @param row : numéro de ligne
         * @return LibraryItem
         */
        LibraryItem *getCardPackAtRow(const int &row) const;

        /**
         * @brief Retourne l'objet contenu à l'index donné.
         *
         * @param index
         * @return LibraryItem
         */
        LibraryItem *getLibraryItemAtIndex(const QModelIndex &index) const;

        /**
         * @brief Lit un fichier JSON correspondant à la bibliothèque entière
         * et crée les objets correspondants.
         *
         * @param json
         */
        void readJson(const QJsonObject &json);
        QJsonObject toJson() const;

    private:
        LibraryItem *rootCard; /** Racine de la bibliothèque */
};

#endif // CARDPACK_H
