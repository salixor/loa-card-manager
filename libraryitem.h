#ifndef CARD_H
#define CARD_H

#include "library.h"

#include <QAbstractItemModel>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QList>

class Library;


/**
 * @brief Modèle représentant un paquet de cartes ou une carte. Par définition de
 * la classe, un paquet ou une carte peut posséder plusieurs objets de type LibraryItem,
 * mais une carte n'aura pas d'objets enfants. L'héritage de QAbstractItemModel s'explique
 * par la volonté de relier directement la vue au modèle.
 *
 */
class LibraryItem : public QAbstractItemModel {
    Q_OBJECT

    public:
        /**
         * @brief Constructeur par défaut. Initialise la racine et les deux premières
         * colonnes (titre et description).
         *
         * @param parent
         */
        LibraryItem(QObject *parent = nullptr);

        /**
         * @brief Constructeur par copie.
         *
         * @param data
         * @param parentCard
         */
        LibraryItem(const QList<QPair<QString, QVariant>> &data, LibraryItem *parentCard = nullptr);

        /**
         * @brief Constructeur qui prend en paramètre les données de la carte.
         *
         * @param other
         * @param parentCard
         */
        LibraryItem(const LibraryItem &other, LibraryItem *parentCard = nullptr);

        /**
         * @brief Destructeur.
         *
         */
        ~LibraryItem();

        /**
         * @brief Opérateur d'égalité pour deux objets de type LibraryItem.
         *
         * @param other
         * @return bool operator
         */
        bool operator==(const LibraryItem& other) const;

        /**
         * @brief Retourne l'index de l'objet avec les paramètres spécifiés.
         *
         * @param row
         * @param column
         * @param parentIndex
         * @return QModelIndex
         */
        QModelIndex index(int row, int column, const QModelIndex &parentIndex) const override;
        /**
         * @brief Retourne le parent de la carte/du paquet à l'index indiqué.
         *
         * @param index
         * @return QModelIndex
         */
        QModelIndex parent(const QModelIndex &index) const override;


        /**
         * @brief Retoune le nombre de cartes contenues dans le paquet, ou 1 dans le cas d'une carte.
         *
         * @param parent
         * @return int
         */
        int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        /**
         * @brief Retoune le nombre de données contenus dans une carte, ou le nombre de
         * colonnes de données d'un paquet.
         *
         * @param parent
         * @return int
         */
        int columnCount(const QModelIndex &parent = QModelIndex()) const override;

        /**
         * @brief Retourne les données associées à l'objet à l'index donné.
         *
         * @param index
         * @param role
         * @return QVariant
         */
        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        /**
         * @brief Modifie les données associées à l'objet à l'index donné.
         * Méthode nécessaire pour l'édition.
         *
         * @param index
         * @param value
         * @param role
         * @return bool
         */
        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

        /**
         * @brief Retourne les flags de l'objet à l'index donné.
         *
         * @param index
         * @return Qt::ItemFlags
         */
        Qt::ItemFlags flags(const QModelIndex &index) const override;

        /**
         * @brief Retourne les données d'en-tête pour le rôle et la section donnés.
         *
         * @param section
         * @param orientation
         * @param role
         * @return QVariant
         */
        QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

        /**
         * @brief Ajoute un enfant (soit une carte, soit un paquet) à l'objet courant.
         *
         * @param childCard
         */
        void appendChild(LibraryItem *childCard);
        /**
         * @brief Supprime un enfant (soit une carte, soit un paquet) à l'objet courant.
         *
         * @param row
         * @return bool
         */
        bool removeChild(const int &row);
        /**
         * @brief Retourne le n-ième élément enfant de l'objet courant.
         *
         * @param row
         * @return LibraryItem
         */
        LibraryItem *child(const int &row);
        /**
         * @brief Retourne le parent de l'objet courant.
         *
         * @return LibraryItem
         */
        LibraryItem *parentCard();

        /**
         * @brief Retourne le nombre d'enfants.
         *
         * @return int
         */
        int row() const;
        /**
         * @brief Retourne les données de la colonne spécifiée.
         *
         * @param column
         * @return QVariant
         */
        QVariant data(int column) const;

        /**
         * @brief Vérifie si l'élément courant est une carte.
         *
         * @return bool
         */
        bool isCard();
        /**
         * @brief Vérifie si l'élément courant est un paquet de cartes.
         *
         * @return bool
         */
        bool isCardPack();

        /**
         * @brief Ajoute des données à l'objet courant.
         *
         * @param fieldName : Intitulé du champ
         * @param data : Donnée sous forme de QVariant
         */
        void addData(const QString &fieldName = QString(), const QVariant &data = QVariant());
        /**
         * @brief Modifie le titre de l'élément courant (colonne 0).
         *
         * @param title
         */
        void setTitle(const QString &title);
        /**
         * @brief Modifie la description de l'élément courant (colonne 1).
         *
         * @param description
         */
        void setDescription(const QString &description);

        /**
         * @brief Retourne l'intitulé de la donnée numéro i de l'élément courant.
         *
         * @param i
         * @return QString
         */
        QString getFieldName(const int &i) const;
        /**
         * @brief Retourne la donnée associée à la colonne i à de l'élément courant.
         *
         * @param i
         * @return QVariant
         */
        QVariant getFieldData(const int &i) const;

        /**
         * @brief Retourne le titre de l'élément courant (colonne 0).
         *
         * @return QString
         */
        QString getTitle() const;
        /**
         * @brief Retourne la description de l'élément courant (colonne 1);
         *
         * @return QString
         */
        QString getDescription() const;

    private:
        /**
         * @brief Ajoute des données à l'objet courant, à la colonne voulue.
         *
         * @param column : Le numéro de colonne du champ
         * @param fieldName : Intitulé du champ
         * @param data : Donnée sous forme de QVariant
         */
        void addDataAt(const int &column = -1, const QString &fieldName = QString(), const QVariant &data = QVariant());

        LibraryItem *parentItem; /** Paquet dans lequel la carte est contenue.*/
        QList<LibraryItem*> children; /** Enfants (cartes) d'un paquet (ou rien si c'est une carte) */
        QList<QPair<QString, QVariant>> itemData; /** Données */
};

#endif // CARD_H
