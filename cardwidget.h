#ifndef CARDWIDGET_H
#define CARDWIDGET_H

#include "libraryitem.h"

#include <QAbstractItemModel>
#include <QDataWidgetMapper>
#include <QVBoxLayout>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QWidget>

/**
 * @brief Classe héritant de QWidget pour l'affichage du contenu des paquets/cartes
 * dans le panel de droite. Un objet QDataWidgetMapper permet de lier le modèle de la
 * bibliothèque à la vue, ce qui permet d'éditer et d'actualiser les données en temps réel.
 *
 */
class CardWidget : public QWidget {
    Q_OBJECT

    public:
        /**
         * @brief Constructeur, initialise le layout, le QDataWidgetMapper et les structures.
         *
         * @param parent QWidget dans lequel on veut afficher la carte
         * @param library Modèle de bibliothèque à utiliser
         */
        CardWidget(QWidget *parent = 0, Library *library = 0);

        /**
         * @brief Destructeur, supprime le pointeur associé au QDataWidgetMapper.
         *
         */
        ~CardWidget();

        /**
         * @brief Modifie la valeur de l'index courant.
         *
         * @param index
         */
        void setCurrentIndex(QModelIndex index);

        void clear();

    private slots:
        void addField();

    signals:
        void libraryElementChanged();

    private:
        /**
         * @brief Supprime le contenu du widget pour le remettre à zéro.
         *
         */
        void resetWidget();
        /**
         * @brief Met en place les champs associés au modèle.
         *
         */
        void setupFields();

        Library *library; /** Modèle de bibliothèque */
        QModelIndex currentIndex = QModelIndex(); /** Index courant */
        QDataWidgetMapper *mapper; /** Association au modèle */

        QVBoxLayout *layout; /** Layout vertical */
        QFont font; /** Police utilisée pour l'affichage des champs */
        QList<QLabel*> labels; /** Labels de champs */
        QList<QLineEdit*> fields; /** Champs d'édition */
        QPushButton *addFieldButton; /** Bouton pour l'ajout de champs */
};


#endif // CARDWIDGET_H
