#include "library.h"

Library::Library(QObject *parent) : QAbstractItemModel(parent) {
    QList<QPair<QString, QVariant>> rootData;
    rootData.append(QPair<QString, QVariant>("Title", "Title"));
    rootData.append(QPair<QString, QVariant>("Description", "Description"));
    rootCard = new LibraryItem(rootData);
}

Library::~Library() {
    delete rootCard;
}

// --------------------------------------------

QModelIndex Library::index(int row, int column, const QModelIndex &parentIndex) const {
    if (!hasIndex(row, column, parentIndex))
        return QModelIndex();

    LibraryItem *parentItem = getLibraryItemAtIndex(parentIndex);
    LibraryItem *childItem = parentItem->child(row);

    if (childItem)
        return createIndex(row, column, childItem);

    return QModelIndex();
}

QModelIndex Library::parent(const QModelIndex &index) const {
    if (!index.isValid())
        return QModelIndex();

    LibraryItem *child = static_cast<LibraryItem*>(index.internalPointer());
    LibraryItem *parent = child->parentCard();

    if (parent == rootCard)
        return QModelIndex();

    return createIndex(parent->row(), 0, parent);
}

// --------------------------------------------

int Library::rowCount(const QModelIndex &parent) const {
    LibraryItem *parentCard;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentCard = rootCard;
    else
        parentCard = static_cast<LibraryItem*>(parent.internalPointer());

    return parentCard->rowCount();
}

int Library::columnCount(const QModelIndex &parent) const {
    if (parent.isValid())
        return static_cast<LibraryItem*>(parent.internalPointer())->columnCount();

    return rootCard->columnCount();
}

// --------------------------------------------

QVariant Library::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    LibraryItem *card = static_cast<LibraryItem*>(index.internalPointer());
    return card->data(index.column());
}

bool Library::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (index.isValid() && role == Qt::EditRole) {
        getLibraryItemAtIndex(index)->setData(index, value, role);
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

void Library::resetModel() {
    if (rowCount() == 0)
        return;

    beginResetModel();
    this->removeRows(0, rowCount());
    endResetModel();
}

// --------------------------------------------

Qt::ItemFlags Library::flags(const QModelIndex &index) const {
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

QVariant Library::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootCard->data(section);

    return QVariant();
}

// --------------------------------------------

// Renvoie l'index de la Card
QModelIndex Library::addCard(const LibraryItem &card, LibraryItem *cardPack) {
    if (!cardPack)
        return QModelIndex();

    QModelIndex cardPackIndex = createIndex(cardPack->row(), 0, cardPack);

    beginInsertRows(cardPackIndex, cardPack->rowCount(), cardPack->rowCount());
    cardPack->appendChild(new LibraryItem(card, cardPack));
    endInsertRows();

    return index(cardPack->rowCount() - 1, 0, cardPackIndex);
}

// Renvoie l'index du CardPack
QModelIndex Library::addCardPack(const LibraryItem &cardPack) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    rootCard->appendChild(new LibraryItem(cardPack, rootCard));
    endInsertRows();

    return index(rowCount() - 1);
}

// --------------------------------------------

bool Library::removeRow(int row, const QModelIndex &parentIndex) {
    return removeRows(row, 1, parentIndex);
}

bool Library::removeRows(int row, int count, const QModelIndex &parentIndex) {
    bool success = false;

    beginRemoveRows(parentIndex, row, row + count - 1);
    for (int i = row + count - 1; i >= row; i--)
        success = getLibraryItemAtIndex(parentIndex)->removeChild(i) || success;
    endRemoveRows();

    return success;
}

// --------------------------------------------

LibraryItem *Library::getCardPackAtRow(const int &row) const {
    return rootCard->child(row);
}

LibraryItem *Library::getLibraryItemAtIndex(const QModelIndex &index) const {
    if (!index.isValid())
        return rootCard;

    LibraryItem *item = static_cast<LibraryItem*>(index.internalPointer());

    if (item)
        return item;

    return rootCard;
}

// --------------------------------------------

void Library::readJson(const QJsonObject &json) {
    if (!json.contains("packs")){
        qWarning("Missing packs in json file … Continuing with no data from json.");
        return;
    }

    // Itération sur les packs de cartes de la librairie
    foreach (const QJsonValue &packInfo, json["packs"].toArray()) {
        QJsonObject packJson = packInfo.toObject();
        LibraryItem pack = new LibraryItem;

        if (packJson.contains("Title")) // Le pack a un titre
            pack.setTitle(packJson["Title"].toString());
        if (packJson.contains("Description")) // Le pack a une description
            pack.setDescription(packJson["Description"].toString());

        // Ajout du pack actuel à la librairie
        LibraryItem *cardPack = static_cast<LibraryItem*>(addCardPack(pack).internalPointer());

        // Itération sur les cartes du pack actuel
        foreach (const QJsonValue &cardInfo, packJson["cards"].toArray()) {
            QJsonObject cardJson = cardInfo.toObject();
            LibraryItem card = new LibraryItem;

            // Itération sur les champs de la carte actuelle
            foreach(const QString &key, cardJson.keys()) {
                if (key == "Title")
                    card.setTitle(cardJson.value(key).toString());
                else if (key == "Description")
                    card.setDescription(cardJson.value(key).toString());
                else
                    card.addData(key, QVariant(cardJson.value(key).toString()));
            }

            // On ajoute des colonnes au CardPack pour en avoir suffisamment pour la carte
            for (int i = 0; i < card.columnCount() + 2 - cardPack->columnCount(); i++)
                cardPack->addData();

            // Ajout de la carte actuelle au pack actuel
            addCard(card, cardPack);
        }
    }
}

QJsonObject Library::toJson() const {
    QJsonObject json;
    QJsonArray packs;

    for (int i = 0; i < rootCard->rowCount(); ++i) {
        LibraryItem *currentPack = rootCard->child(i);
        QJsonObject packJson;
        QJsonArray cards;

        // Données du paquet
        for (int iField = 0; iField < currentPack->columnCount(); ++iField) {
            if (currentPack->getFieldName(iField) == "")
                continue;

            packJson[currentPack->getFieldName(iField)] = QJsonValue(QJsonValue::fromVariant(currentPack->getFieldData(iField)));
        }

        // Cartes du paquet
        for (int numCard = 0; numCard < currentPack->rowCount(); ++numCard) {
            LibraryItem *currentCard = currentPack->child(numCard);
            QJsonObject cardJson;

            // Données de la carte
            for (int i = 0; i < currentCard->columnCount(); ++i)
                cardJson[currentCard->getFieldName(i)] = QJsonValue(QJsonValue::fromVariant(currentCard->getFieldData(i)));

            cards.append(cardJson);
        }

        packJson["cards"] = cards;
        packs.append(QJsonValue(packJson));
    }

    json["packs"] = packs;
    return json;
}
