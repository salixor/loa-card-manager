#include "cardwidget.h"
#include "library.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QJsonDocument>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    library = new Library;

    // Définition de la Card par défaut
    defaultCard.setTitle("New card");
    defaultCard.setDescription("Please provide field values");

    // Définition du CardPack par défaut
    defaultCardPack.setTitle("New pack");
    defaultCardPack.setDescription("Please provide field values");

    // Setup de l'UI
    ui->setupUi(this);
    setWindowTitle("Flashcard Manager");
    ui->treeView->setModel(library);
    ui->treeView->expandAll();

    // Le TreeView est mis à la taille minimale
    ui->splitter->setStretchFactor(0, 0);
    ui->splitter->setStretchFactor(1, 1);

    // Ajout du label indiquant le fichier ouvert
    jsonOpenFileLabel = new QLabel(ui->statusBar);
    jsonOpenFileLabel->setText(QString("No JSON file opened …"));
    ui->statusBar->addWidget(jsonOpenFileLabel);

    // Ajout du label indiquant le nombre de paquets de cartes
    packsCountLabel = new QLabel(ui->statusBar);
    packsCountLabel->setText(QString("Pack count: ") + QString::number(packsCount));
    ui->statusBar->addPermanentWidget(packsCountLabel);

    // Ajout du label indiquant le nombre de cartes
    cardsCountLabel = new QLabel(ui->statusBar);
    cardsCountLabel->setText(QString("Card count: " + QString::number(cardsCount)));
    ui->statusBar->addPermanentWidget(cardsCountLabel);

    // Setup de la vue de droite de l'UI
    cardFront = new CardWidget(ui->rightSide, library);
    ui->rightSide->addWidget(cardFront);
    ui->rightSide->addWidget(cardBack);

    // Importation du précédent fichier ouvert
    if (settings.contains("data/json")) {
        setCurrentJSONFile(settings.value("data/json").toString());
        importJson(currentJSONFilename); // Import des données du fichier JSON
    }

    updateAll(); // On met tout à jour

    // Connection entre la sélection et la fonction qui traite les changements de sélection
    connect(ui->treeView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
            this, SLOT(selectionChanged(const QItemSelection&, const QItemSelection&)));

    // Connection entre la modification d'un élément et l'état de modification du modèle
    connect(cardFront, SIGNAL(libraryElementChanged()), this, SLOT(setModified()));

    // Connection entre la modification d'un élément et les mises à jour des boutons
    connect(cardFront, SIGNAL(libraryElementChanged()), this, SLOT(updateButtons()));

    // Connection entre la modification d'un élément et les mises à jour de la barre de status
    connect(cardFront, SIGNAL(libraryElementChanged()), this, SLOT(updateStatusBar()));
}

MainWindow::~MainWindow() {
    delete ui;
    delete library;
}

// Mise à jour de l'état de modification du modèle
void MainWindow::setModified(bool modified) {
    isModified = modified;
}

// Changement du fichier JSON actuel
void MainWindow::setCurrentJSONFile(QString filename) {
    currentJSONFilename = filename;
    settings.setValue("data/json", currentJSONFilename);
    updateStatusBar(); // Mise à jour de la barre d'état
}

// --------------------------------------------

// Vérifie qu'un CardPack ne correspond pas à un CardPack par défaut
bool MainWindow::isDefaultCardPack(LibraryItem *item) {
    if (item->isCardPack())
        return (*item == defaultCardPack);
    return false;
}

// Vérifie qu'une Card ne correspond pas à une Card par défaut
bool MainWindow::isDefaultCard(LibraryItem *item) {
    if (item->isCard())
        return (*item == defaultCard);
    return false;
}

// --------------------------------------------

void MainWindow::setAddCardEnabled(const bool &enabled) {
    ui->addCardButton->setEnabled(enabled);
    ui->actionAdd_card->setEnabled(enabled);
}

void MainWindow::setAddCardPackEnabled(const bool &enabled) {
    ui->addCardPackButton->setEnabled(enabled);
    ui->actionAdd_card_pack->setEnabled(enabled);
}

// --------------------------------------------

// Ajout d'une nouvelle Card
void MainWindow::on_addCardButton_clicked() {
    if (!ui->treeView->currentIndex().isValid())
        return;

    // On vérifie qu'une nouvelle Card n'a pas déjà été ajoutée, mais non modifiée
    LibraryItem *currentSelectedItem = library->getLibraryItemAtIndex(ui->treeView->currentIndex());
    if (isDefaultCard(currentSelectedItem))
        return;

    LibraryItem *cardPack;

    if (currentSelectedItem->isCard()) // L'élément sélectionné est une Card
        cardPack = currentSelectedItem->parentCard();
    else // L'élément sélectionné est un CardPack
        cardPack = currentSelectedItem;

    // On ajoute une nouvelle Card et on déplace la sélection
    QModelIndex cardIndex = library->addCard(defaultCard, cardPack);
    ui->treeView->setCurrentIndex(cardIndex);

    updateAll(); // On met tout à jour

    // On met à jour la possibilité de cliquer sur les différents boutons
    setAddCardPackEnabled(true);
    setAddCardEnabled(false);
}

void MainWindow::on_actionAdd_card_triggered() {
    on_addCardButton_clicked();
}

// --------------------------------------------

// Ajout d'un nouveau CardPack
void MainWindow::on_addCardPackButton_clicked() {
    // On vérifie qu'un nouveau CardPack n'a pas déjà été ajouté, mais non modifié
    LibraryItem *currentSelectedItem = library->getLibraryItemAtIndex(ui->treeView->currentIndex());
    if (isDefaultCardPack(currentSelectedItem))
        return;

    // On ajoute un nouveau CardPack et on déplace la sélection
    QModelIndex packIndex = library->addCardPack(defaultCardPack);
    ui->treeView->setCurrentIndex(packIndex);

    updateAll(); // On met tout à jour

    // On met à jour la possibilité de cliquer sur les différents boutons
    setAddCardPackEnabled(false);
    setAddCardEnabled(false);
}

void MainWindow::on_actionAdd_card_pack_triggered() {
    on_addCardPackButton_clicked();
}

// --------------------------------------------

// Suppression d'un CardPack ou d'une Card
void MainWindow::on_deleteButton_clicked() {
    if (!ui->treeView->currentIndex().isValid())
        return;

    QModelIndex currentIndex = ui->treeView->currentIndex();
    bool success = library->removeRow(currentIndex.row(), currentIndex.parent());

    if (!success)
        return;

    setModified(true); // Le modèle a une modification
    updateAll(); // On met tout à jour
}

// --------------------------------------------

// Traitement de changement dans la sélection
void MainWindow::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected) {
    // Rien n'est sélectionné : on rend les boutons non cliquables
    if (selected.indexes().count() == 0) {
        setAddCardEnabled(false);
        ui->deleteButton->setEnabled(false);
        return;
    }

    // Si quelque chose est déselectionné, on vérifie qu'il a bien été édité
    // et si il n'a pas été édité, alors on le supprime du modèle
    if (deselected.indexes().count() > 0) {
        QModelIndex previouslySelectedIndex = deselected.indexes().at(0);
        LibraryItem *previousSelectedItem = library->getLibraryItemAtIndex(previouslySelectedIndex);

        if (isDefaultCardPack(previousSelectedItem) || isDefaultCard(previousSelectedItem)) {
            library->removeRow(previouslySelectedIndex.row(), previouslySelectedIndex.parent());
            setModified(false); // Le modèle n'a aucune modification
        }
    }

    updateAll(); // On met tout à jour
}

// --------------------------------------------

// Mises à jour
void MainWindow::updateAll() {
    updateCardDisplay();
    updateButtons();
    updateStatusBar();
}

// Mise à jour de la vue de la carte à droite
void MainWindow::updateCardDisplay() {
    if (!ui->treeView->currentIndex().isValid()) {
        cardFront->clear();
        return;
    }

    cardFront->setCurrentIndex(ui->treeView->currentIndex());
}

// Mets à jour les boutons
void MainWindow::updateButtons() {
    LibraryItem *currentItem = library->getLibraryItemAtIndex(ui->treeView->currentIndex());

    setAddCardPackEnabled(true);
    setAddCardEnabled(true);

    if (currentItem->isCard()) {
        if (isDefaultCard(currentItem))
            setAddCardEnabled(false);
    }
    else if (currentItem->isCardPack()) {
        if (isDefaultCardPack(currentItem)) {
            setAddCardEnabled(false);
            setAddCardPackEnabled(false);
        }
    }

    // Mise à jour du bouton de suppression
    ui->deleteButton->setEnabled(library->rowCount() != 0);

    // Mise à jour du bouton de sauvegarde du JSON
    ui->actionSave_JSON->setEnabled(isModified);

    // Mise à jour du bouton de fermeture du JSON
    ui->actionClose_JSON->setEnabled(!currentJSONFilename.isEmpty());

    // Mise à jour du bouton de reset de la librairie
    ui->actionReset_library->setEnabled(library->rowCount() != 0);
}

// Mets à jour la barre d'état
void MainWindow::updateStatusBar(QString specialMessage) {
    // Mise à jour du nombre de paquets de cartes et du nombre de cartes
    packsCount = library->rowCount();

    LibraryItem *currentSelectedItem = library->getLibraryItemAtIndex(ui->treeView->currentIndex());

    cardsCount = 0;
    if (currentSelectedItem->isCard()) // L'élément sélectionné est une Card
        cardsCount = currentSelectedItem->parentCard()->rowCount();
    if (currentSelectedItem->isCardPack()) // L'élément sélectionné est un CardPack
        cardsCount = currentSelectedItem->rowCount();

    packsCountLabel->setText(QString("Pack count: ") + QString::number(packsCount));
    cardsCountLabel->setText(QString("Card count: " + QString::number(cardsCount)));

    // Mise à jour du nom du fichier JSON actuellement ouvert
    QString openFileLabel = currentJSONFilename;
    if (openFileLabel.isEmpty())
        openFileLabel = QString("No JSON file opened …");
    else if (!specialMessage.isEmpty())
        openFileLabel = QString("[") + specialMessage + QString("] ") + openFileLabel;
    else if (isModified)
        openFileLabel = QString("[Not saved] ") + openFileLabel;

    jsonOpenFileLabel->setText(openFileLabel);
}

// --------------------------------------------

// Import d'un fichier JSON dans la librairie
void MainWindow::importJson(const QString &filename) {
    QFile file(filename);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString val = file.readAll();
    file.close();

    QJsonObject jsonObj = QJsonDocument::fromJson(val.toUtf8()).object();
    library->readJson(jsonObj);
}

// Export de la librairie vers un JSON
void MainWindow::exportJson(const QString &filename, const QJsonObject json) {
    QFile file(filename);
    file.open(QIODevice::WriteOnly | QIODevice::Text);

    QJsonDocument jsonDoc(json);
    file.write(jsonDoc.toJson());
}

// --------------------------------------------

void MainWindow::on_actionOpen_JSON_triggered() {
    // Demande du fichier à ouvrir à l'utilisateur
    QString filename = QFileDialog::getOpenFileName(this, tr("Open JSON"), ".", tr("JSON Files (*.json)"));
    if (filename.isEmpty())
        return;

    setCurrentJSONFile(filename); // Mise à jour du nom du fichier actuel
    library->resetModel(); // Remise à zéro du modèle
    importJson(filename); // Import des données du fichier JSON
    setModified(false); // Le modèle n'a aucune une modification
    updateAll(); // On met tout à jour
}

void MainWindow::on_actionImport_JSON_triggered() {
    QString filename = QFileDialog::getOpenFileName(this, tr("Import JSON"), ".", tr("JSON Files (*.json)"));
    if (filename.isEmpty())
        return;

    importJson(filename); // Import des données du fichier JSON
    setModified(true); // Le modèle a une modification
    updateAll(); // On met tout à jour
}

void MainWindow::on_actionSave_JSON_triggered() {
    if (currentJSONFilename.isEmpty()) {
        ui->actionSave_JSON->setEnabled(false);
        on_actionExport_JSON_triggered();
        return;
    }

    setCurrentJSONFile(currentJSONFilename); // Mise à jour du nom du fichier actuel
    exportJson(currentJSONFilename, library->toJson()); // Export des données vers un fichier JSON

    setModified(false); // Le modèle n'a aucune une modification
    updateButtons(); // Mise à jour des boutons
    updateStatusBar(QString("Just saved !")); // Mise à jour de la barre d'état
}

void MainWindow::on_actionExport_JSON_triggered() {
    // Demande du fichier vers lequel exporter à l'utilisateur
    QString filename = QFileDialog::getSaveFileName(this, tr("Export JSON"), ".", tr("JSON Files (*json)"));
    if (filename.isEmpty())
        return;

    setCurrentJSONFile(filename); // Mise à jour du nom du fichier actuel
    exportJson(currentJSONFilename, library->toJson()); // Export des données vers un fichier JSON

    setModified(false); // Le modèle n'a aucune une modification
    updateButtons(); // Mise à jour des boutons
    updateStatusBar(QString("Just exported !")); // Mise à jour de la barre d'état
}

// --------------------------------------------

void MainWindow::on_actionClose_JSON_triggered() {
    // Si le modèle est modifié, on propose d'enregistrer
    if (isModified) {
        QMessageBox msgBox;
        msgBox.setText("Some changes have not been saved.");
        msgBox.setInformativeText("Do you want to save your changes ?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);

        switch (msgBox.exec()) {
            case QMessageBox::Save:
                on_actionSave_JSON_triggered();
                break;

            case QMessageBox::Cancel:
                return;
                break;

            default:
                break;
        }
    }

    setCurrentJSONFile(); // On supprime le nom de fichier actuel
    library->resetModel(); // Remise à zéro du modèle

    setModified(false); // Le modèle n'est plus modifié
    updateAll(); // On met tout à jour
}

void MainWindow::on_actionReset_library_triggered() {
    // On prévient que les modifications seront perdues
    QMessageBox msgBox;
    msgBox.setText("Some changes have not been saved.");
    msgBox.setInformativeText("If you proceed, you'll lose this data.");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);

    switch (msgBox.exec()) {
        case QMessageBox::Ok:
            library->resetModel(); // Remise à zéro du modèle
            on_actionSave_JSON_triggered();
            break;

        case QMessageBox::Cancel:
            return;
            break;

        default:
            break;
    }

    setModified(false); // Le modèle n'est plus modifié
    updateAll(); // On met tout à jour
}
